package cql.ecci.ucr.ac.cr.mapasucr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {

    private FusedLocationProviderClient fusedLocationClient;
    private GoogleMap mMap;
    private GoogleApi mGoogleApiClient;
    private static final int LOCATION_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            mMap.setMyLocationEnabled(true);
        }else{
            solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST_CODE);
        }
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    protected void solicitarPermiso(String permissionType, int requestCode){
        ActivityCompat.requestPermissions(this, new String[] {permissionType}, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults){
        switch(requestCode){
            case LOCATION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Permission granted
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng){
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        Toast.makeText(getApplicationContext(), latLng.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapLongClick(LatLng latLng){
        mMap.addMarker(new MarkerOptions()
            .position(latLng)
            .title(latLng.toString())
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        Toast.makeText(getApplicationContext(), latLng.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_1){
            marcadorSodaUCR();
            return true;
        }else if(id == R.id.action_2){
            mostrarZoom();
            return true;
        }else if(id == R.id.action_3){
            posicionCamaraZoom();
            return true;
        }else if(id == R.id.action_4){
            MapaSatelite();
            return true;
        }else if(id == R.id.action_5){
            MapaHibrido();
            return true;
        }else if(id == R.id.action_6){
            MapaNinguno();
            return true;
        }else if(id == R.id.action_7){
            MapaNormal();
            return true;
        }else if(id == R.id.action_8){
            MapaTierra();
            return true;
        }else if(id == R.id.action_9){
            obtenerLocalizacion();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void marcadorSodaUCR(){
        if(mMap != null){
            LatLng sodaOdonto = new LatLng(9.938035, -84.051509);
            mMap.addMarker(new MarkerOptions().position(sodaOdonto).title("Soda de Odontología de la UCR"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sodaOdonto));
        }
    }

    public void mostrarZoom(){
        UiSettings mapSettings;
        mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true);
    }

    public void posicionCamaraZoom(){
        LatLng sodaOdonto = new LatLng(9.938035, -84.051509);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sodaOdonto)
                .zoom(15)
                .bearing(70)
                .tilt(25)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void MapaSatelite(){
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    public void MapaHibrido(){
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void MapaNinguno(){
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
    }

    public void MapaNormal(){
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    public void MapaTierra(){
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    private void obtenerLocalizacion(){
        if(mMap == null){
            Toast.makeText(getApplicationContext(), "No se pudo obtener la ubicación actual.", Toast.LENGTH_LONG).show();
            return;
        }

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            return;
        }

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                                    .zoom(15)
                                    .bearing(70)
                                    .tilt(25)
                                    .build(); mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                });
    }
}
